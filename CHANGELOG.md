CHANGELOG
=========

6.1.0
-----
- Introduction des propriétés applicationConfigPath et applicationConfigOverrides dans AbstractControllerTestCase 

6.0.0
-----
- Requiert les nouvelles bibliothèques unicaen/authentification et privilege.

5.0.0
-----
- PHP 8 requis
