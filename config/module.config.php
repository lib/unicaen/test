<?php

namespace UnicaenTest;

return [
    'router'          => [
        'routes' => [
        ],
    ],
    'service_manager' => [
        'factories'  => [
        ],
        'invokables' => [
        ],
    ],
    'controllers'     => [
        'invokables' => [
        ],
    ],
    'navigation'      => [
        'default' => [
        ],
    ],
];
