<?php

namespace UnicaenTest\Entity\Db;

use Doctrine\ORM\EntityManager;
use SplStack;
use Laminas\Stdlib\ArrayUtils;

/**
 * Description of EntityProvider
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
abstract class AbstractEntityProvider
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @var array
     */
    protected $config = [];

    /**
     * @var array
     */
    protected $newEntities;

    /**
     *
     * @param EntityManager $entityManager
     * @param array         $config
     */
    public function __construct(EntityManager $entityManager, array $config = [])
    {
        $this->entityManager = $entityManager;

        $this->config = ArrayUtils::merge($this->config, $config);

        //Asset::setSource($this->getSource());

        $this->newEntities = new SplStack();
    }

    /**
     * @return EntityManager
     */
    public function getEntityManager()
    {
        return $this->entityManager;
    }

    /**
     * @param EntityManager $entityManager
     * @return AbstractEntityProvider
     */
    public function setEntityManager($entityManager)
    {
        $this->entityManager = $entityManager;
        return $this;
    }

    /**
     * @var string
     */
    protected $testClassName;

    /**
     *
     * @param string $className
     * @return $this
     */
    public function setTestClassName($className)
    {
        $this->testClassName = $className;

        return $this;
    }

    /**
     * SUpprime du gestionnaire d'entité les éventuelles nouvelles instances d'entités créées.
     *
     * @param bool $flush
     * @return $this
     */
    public function removeNewEntities($flush = true)
    {
        $this->newEntities->rewind();

        while ($this->newEntities->valid()) {
            $entity = $this->newEntities->current();
//            if ($entity instanceof ElementPedagogique) {
//                // On historise les EP à la main car on n'arrive pas à éviter l'erreur suivante :
//                // Doctrine\DBAL\DBALException: An exception occurred while executing
//                // 'DELETE FROM V_ELEMENT_TYPE_INTERVENTION WHERE ELEMENT_PEDAGOGIQUE_ID = ?' with params [17970]:
//                // ORA-01752: cannot delete from view without exactly one key-preserved table
//                $entity->setHistoDestruction(new \DateTime());
//            }
//            else {
                $this->entityManager->remove($entity);
//            }
            $this->newEntities->next();
        }

        if ($flush) {
            $this->entityManager->flush();
        }

        return $this;
    }
}