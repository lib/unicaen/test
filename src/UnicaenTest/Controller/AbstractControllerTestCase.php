<?php

namespace UnicaenTest\Controller;

use Doctrine\ORM\EntityManager;
use Laminas\ServiceManager\ServiceManager;
use Laminas\Stdlib\ArrayUtils;
use Prophecy\Argument;
use Prophecy\Prophecy\ObjectProphecy;
use Prophecy\Prophet;
use Psr\Container\ContainerExceptionInterface;
use RuntimeException;

/**
 * Class AbstractControllerTest
 *
 * @package ApplicationTest\Controller
 */
abstract class AbstractControllerTestCase extends \Laminas\Test\PHPUnit\Controller\AbstractControllerTestCase
{
    /**
     * Chemin vers le fichier de config 'application.config.php'.
     *
     * @var string Ex : __DIR__ . '/../../../../config/application.config.php'
     */
    protected string $applicationConfigPath;

    /**
     * The module configuration should still be applicable for tests.
     * You can override configuration here with test case specific values,
     * such as sample view templates, path stacks, module_listener_options, etc.
     *
     * @var array
     */
    protected array $applicationConfigOverrides = [];


    public function setUp(): void
    {
        $this->setApplicationConfig(ArrayUtils::merge(
        // Grabbing the full application configuration:
            include $this->applicationConfigPath,
            $this->applicationConfigOverrides
        ));

        parent::setUp();
    }

    /**
     * @param string $authorizeServiceClass
     * @return ObjectProphecy|\UnicaenPrivilege\Service\AuthorizeService
     */
    protected function mockAuthorizeService(string $authorizeServiceClass)
    {
        $prophet = new Prophet();

        /** @var ObjectProphecy|\UnicaenPrivilege\Service\AuthorizeService $prophecy */
        $prophecy = $prophet->prophesize();
        $prophecy->willExtend($authorizeServiceClass);

        $service = $prophecy->reveal();

        /* @var $services ServiceManager */
        $services = $this->getApplication()->getServiceManager();
        $services->setAllowOverride(true);
        $services->setFactory('BjyAuthorize\Service\Authorize', function() use ($service) {
            return $service;
        });

        return $prophecy;
    }

    /**
     * @param string $userContextServiceClass
     * @return ObjectProphecy|\UnicaenAuthentification\Service\UserContext
     */
    protected function mockUserContextService(string $userContextServiceClass)
    {
        $prophet = new Prophet();

        /** @var ObjectProphecy|\UnicaenAuthentification\Service\UserContext $prophecy */
        $prophecy = $prophet->prophesize();
        $prophecy->willExtend($userContextServiceClass);

        $prophecy->getEventManager()->willReturn($this->getApplication()->getEventManager());
        $prophecy->setServiceLocator(Argument::any())->willReturn();

        $service = $prophecy->reveal();

        /* @var $services ServiceManager */
        $services = $this->getApplication()->getServiceManager();
        $services->setAllowOverride(true);
        $services->setFactory(\UnicaenAuthentification\Service\UserContext::class, function() use ($service) {
            return $service;
        });

        return $prophecy;
    }

    protected ?EntityManager $em = null;

    protected function em(string $name = 'orm_default'): EntityManager
    {
        if (null === $this->em) {
            try {
                $this->em = $this->getApplicationServiceLocator()->get("doctrine.entitymanager.$name");
            } catch (ContainerExceptionInterface $e) {
                throw new RuntimeException("Impossible d'obtenir l'entity manager $name !", null, $e);
            }
        }

        return $this->em;
    }
}